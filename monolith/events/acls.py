import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(query):
    """
    #Get the URL of a picture from the Pexels API.
    """
    url = f"https://api.pexels.com/v1/search?query={query}"
    headers = {"Authorization": PEXELS_API_KEY}

    response = requests.get(url, headers=headers)
    pic = response.json()
    return pic["photos"][0]["src"]["original"]


def get_weather_data(q):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={q}&limit=1&appid={OPEN_WEATHER_API_KEY}"
    cord_response = requests.get(url)

    cord = cord_response.json()[0]
    cord_lat = cord["lat"]
    cord_long = cord["lon"]

    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={cord_lat}&lon={cord_long}&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(weather_url)
    weather = response.json()
    weather_dict = {
        "description": weather["weather"][0]["description"],
        "temp": weather["main"]["temp"],
    }
    return weather_dict
